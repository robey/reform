/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#pragma once

#include <stdbool.h>

// battery info reported by the controller, parsed:
typedef struct {
    uint8_t decivolts[8];  // volts of each battery (times 10)
    float total_amps;
    float total_volts;
    uint8_t total_percent;  // 0 - 100
} battery_info_t;

void controller_init(void);
const char *controller_request(const char *command);
bool controller_probe_batteries(battery_info_t *info);
bool controller_battery_is_low(void);
void controller_turn_on_som(void);
void controller_turn_off_som(void);
void controller_reset_som(void);
void controller_wake_som(void);
void controller_turn_off_keyboard(void);
void controller_turn_off_aux(void);
void controller_turn_on_aux(void);
void controller_enable_som_uart(void);
void controller_disable_som_uart(void);
