/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#pragma once
#include <stdbool.h>
#include <stdint.h>

// for a 6x8 font.
// (if you want to use a 4x6 font, you could get 32x5 but it might be unreadable.)
#define MAX_WIDTH 21
#define MAX_HEIGHT 4

/*
 * Monospace font.
 * Each glyph byte is a column of pixels, left to right, with each byte
 * having LSB at the top. Width and height are in pixels.
 */
typedef struct {
  const void *glyphs;
  uint8_t height;
  uint8_t width;
} monospace_font_t;

extern const monospace_font_t font_normal;
extern const monospace_font_t font_bizcat;

void display_init(const monospace_font_t *font);
void display_clear(void);
void display_clear_line(uint8_t line);
void display_invert_line(uint8_t line);
uint8_t display_text_width(void);
uint8_t display_text_height(void);
void display_scroll(void);
void display_putc(uint8_t c);
void display_write_len(const char *text, int len);
void display_write(const char *text);
void display_move(uint8_t x, uint8_t y);
void display_invert(bool invert);
void display_refresh(void);
void display_flush(void);
void display_bitmap(uint16_t offset, const uint8_t *data, uint16_t len);
void display_packed_bitmap(uint16_t offset, const uint8_t *data, uint16_t len);
void display_packed_bitmap_pgm(uint16_t offset, const uint8_t *data, uint16_t len, bool cursor_effect);
void display_blit_packed_bitmap_pgm(const uint8_t *data, uint16_t len);
