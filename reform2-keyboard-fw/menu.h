/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#pragma once

#include <stdint.h>

typedef struct {
    const char *label;          // left side of screen
    const char *key_label;      // right side of screen
    uint8_t keycode;
    bool terminate_menu;        // if false, we stay in menu mode afterwards
    void (*action)(void);
} menu_item_t;
#define MENU_END { NULL, NULL, 0, false, NULL }

void menu_start(const menu_item_t *menu);
void menu_stop(void);
bool menu_is_active(void);
bool menu_key(uint8_t key);
