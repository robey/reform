/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#include <stdio.h>
#include <stdlib.h>
#include "assets/images.h"
#include "controller.h"
#include "display.h"
#include "keyboard.h"
#include "ssd1306.h"
#include "ux.h"

static uint8_t oled_brightness = 0;

// track this so the main loop can clear the screen after some time has passed
static bool display_on = false;

void ux_increase_oled_brightness(void) {
    if (oled_brightness < 0xff) oled_brightness += 0x11;
    oled_contrast(oled_brightness);
}

void ux_decrease_oled_brightness(void) {
    if (oled_brightness > 0) oled_brightness -= 0x11;
    oled_contrast(oled_brightness);
}

// battery fullness icons: 0-5 are left, 6-11 are right
// these macros assume 0 - 100:
#define BATTERY_LEFT(_n) ((_n) >= 45 ? 5 : (_n) < 5 ? 0 : ((_n) - 5) / 10 + 1)
#define BATTERY_RIGHT(_n) ((_n) < 55 ? 6 : (_n) >= 95 ? 11 : ((_n) - 55) / 10 + 7)

static uint8_t decivolts_to_percent(uint8_t decivolts) {
    if (decivolts >= 33) return 100;
    if (decivolts >= 31) return 75;
    if (decivolts >= 30) return 50;
    if (decivolts >= 29) return 25;
    return 0;
}

// will fill 2 bytes
void ux_get_battery_icon(char *buffer, uint8_t percent) {
    buffer[0] = BATTERY_LEFT(percent);
    buffer[1] = BATTERY_RIGHT(percent);
}

static const char *show_one_battery(uint8_t decivolts) {
    static char buffer[7];
    uint8_t percent = decivolts_to_percent(decivolts);
    ux_get_battery_icon(buffer, percent);
    buffer[2] = ' ';
    buffer[3] = '0' + (decivolts / 10);
    buffer[4] = '.';
    buffer[5] = '0' + (decivolts % 10);
    buffer[6] = 0;
    return buffer;
}

void ux_show_battery(void) {
    battery_info_t info;
    controller_probe_batteries(&info);

    // plot, using oranj (6x8) so all the info will fit
    display_init(&font_normal);
    char str[32];
    sprintf(str, "%s  %s   %3d%%",
        show_one_battery(info.decivolts[0]), show_one_battery(info.decivolts[4]), info.total_percent);
    display_write(str);
    sprintf(str, "%s  %s       ",
        show_one_battery(info.decivolts[1]), show_one_battery(info.decivolts[5]));
    display_write(str);
    sprintf(str, "%s  %s %5.2fA",
        show_one_battery(info.decivolts[2]), show_one_battery(info.decivolts[6]), info.total_amps);
    display_write(str);
    sprintf(str, "%s  %s %5.2fV",
        show_one_battery(info.decivolts[3]), show_one_battery(info.decivolts[7]), info.total_volts);
    display_write(str);
    display_flush();
    display_on = true;
}

void ux_show_status(void) {
    display_init(&font_normal);

#ifndef KBD_VARIANT_STANDALONE
    const char *response = controller_request("s");
    display_write(response);
#endif

    display_move(0, 2);
    display_write("MNT Reform Keyboard");
    display_move(0, 3);
    display_write(KBD_FW_REV);
    display_flush();
    display_on = true;
}

void ux_blink_low_battery(bool blink) {
    if (blink) {
        display_blit_packed_bitmap_pgm(battery_warn_bitmap, sizeof(battery_warn_bitmap));
    } else {
        display_refresh();
    }
}

// display the boot logo
void ux_splash(void) {
    display_clear();
    display_flush();
    display_packed_bitmap_pgm(0, logo_bitmap, sizeof(logo_bitmap), true);
    for (int i = 0; i < 0xff; i++) {
        oled_contrast(i);
        Delay_MS(2);
    }
    for (int i = 0; i < 0xff; i++) {
        oled_contrast(0xff - i);
        Delay_MS(2);
    }
    display_on = true;
}

// wipe away the boot logo
void ux_unsplash(void) {
    display_packed_bitmap_pgm(0, logo_bitmap, sizeof(logo_bitmap), false);
    display_packed_bitmap_pgm(0, empty_bitmap, sizeof(empty_bitmap), true);
    display_on = false;
}

bool ux_is_on(void) {
    return display_on;
}

void ux_clear(void) {
    display_clear();
    display_flush();
    display_on = false;
}
