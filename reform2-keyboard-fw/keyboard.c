/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
*/
/*
             LUFA Library
     Copyright (C) Dean Camera, 2018.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/
/*
  Copyright 2018  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

// just for vscode
#ifndef __AVR_ATmega32U4__
#define __AVR_ATmega32U4__
#endif

#include <stdlib.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include "Config/LUFAConfig.h"
#include "LUFA/Drivers/Peripheral/Serial.h"
#include "controller.h"
#include "display.h"
#include "hid_report.h"
#include "keyboard.h"
#include "menu.h"
#include "scancodes.h"
#include "ssd1306.h"
#include "ux.h"

//#define KBD_VARIANT_STANDALONE
#define KBD_VARIANT_QWERTY_US
//#define KBD_VARIANT_NEO2

// check for a low battery every 30 seconds
#define BATTERY_CHECK_MSEC      30000
// how long should the on/off cycle of the low battery alert be
#define BATTERY_ALERT_ON_MSEC   500
#define BATTERY_ALERT_OFF_MSEC  4500

// how long to let the menu display before idling out
#define MENU_IDLE_MSEC          15000
// same, but for UX displays like the keyboard status or the splash screen
#define UX_IDLE_MSEC            15000

/** Buffer to hold the previously generated Keyboard HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Keyboard_HID_Interface = {
    .Config = {
        .InterfaceNumber              = INTERFACE_ID_Keyboard,
        .ReportINEndpoint             = {
            .Address              = KEYBOARD_EPADDR,
            .Size                 = KEYBOARD_EPSIZE,
            .Banks                = 1,
        },
        .PrevReportINBuffer           = PrevKeyboardHIDReportBuffer,
        .PrevReportINBufferSize       = sizeof(PrevKeyboardHIDReportBuffer),
    },
};

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

#define MAX_CONCURRENT_KEYS     6

#define MATRIX_ROWS             6
#define MATRIX_COLUMNS          14
#define MATRIX_BYTES            ((MATRIX_ROWS * MATRIX_COLUMNS + 7) >> 3)

uint8_t matrix[MATRIX_ROWS * MATRIX_COLUMNS] = {
    KEY_ESCAPE, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12, KEY_CIRCLE,

    KEY_GRAVE_ACCENT_AND_TILDE, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0, KEY_MINUS_AND_UNDERSCORE, KEY_EQUAL_AND_PLUS, KEY_BACKSPACE,

    KEY_TAB, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_OPENING_BRACKET_AND_OPENING_BRACE, KEY_CLOSING_BRACKET_AND_CLOSING_BRACE, KEY_BACKSLASH_AND_PIPE,

    HID_KEYBOARD_SC_LEFT_CONTROL, HID_KEYBOARD_SC_APPLICATION, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L, KEY_SEMICOLON_AND_COLON, KEY_APOSTROPHE_AND_QUOTE, KEY_ENTER,

    HID_KEYBOARD_SC_LEFT_SHIFT, HID_KEYBOARD_SC_NON_US_BACKSLASH_AND_PIPE, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M, HID_KEYBOARD_SC_COMMA_AND_LESS_THAN_SIGN, HID_KEYBOARD_SC_DOT_AND_GREATER_THAN_SIGN, KEY_SLASH_AND_QUESTION_MARK,  HID_KEYBOARD_SC_UP_ARROW, HID_KEYBOARD_SC_RIGHT_SHIFT,

    HID_KEYBOARD_SC_RIGHT_GUI, HID_KEYBOARD_SC_LEFT_GUI, HID_KEYBOARD_SC_RIGHT_CONTROL, KEY_SPACE, HID_KEYBOARD_SC_LEFT_ALT, HID_KEYBOARD_SC_RIGHT_ALT, KEY_SPACE, HID_KEYBOARD_SC_PAGE_UP, HID_KEYBOARD_SC_PAGE_DOWN, HID_KEYBOARD_SC_LEFT_ARROW, HID_KEYBOARD_SC_DOWN_ARROW, HID_KEYBOARD_SC_RIGHT_ARROW, 0, 0,
};

#define CIRCLE_BYTE             0x01
#define CIRCLE_BIT              (1 << 5)

static uint8_t state[MATRIX_BYTES] = { 0, };

#ifndef KBD_VARIANT_STANDALONE
static bool low_battery = false;
#endif

volatile static uint32_t timer_tick = 0;

ISR(TIMER0_OVF_vect) {
    timer_tick++;
}

uint32_t timer_get_ticks(void) {
    return timer_tick;
}

// rough estimate!
// don't use for timekeeping, only for human-perceivable delays.
uint32_t timer_diff_msec(uint32_t start_ticks, uint32_t end_ticks) {
    return (end_ticks - start_ticks) >> 5;
}

uint8_t pwmval = 8;

void kbd_brightness_init(void) {
    // initial brightness
    OCR0A = pwmval;

    // configure timer 0:
    //   - WGM0 = 001: phase-correct PWM
    //   - COM0A = 10: clear when counting up, set when counting down
    //   - CS0 = 001: no prescaling, clock is 16MHz
    //   - TOIE0 = 1: trigger interrupt at 0
    // interrupts happen after every complete phase (256 up, 256 down), so
    // each interrupt is 16MHz / (256 * 2) = 32KHz, which we track in a 32-bit
    // counter, which will roll over about every 1.5 days.

    // 7:6 = COM0A, 1:0 = WGM0 1:0 (WGM0 is split across 2 registers)
    TCCR0A = (1 << 7) | (0 << 6) | (0 << 1) | 1;
    // 3 = WGM0 2, 2:0 = CS0
    TCCR0B = (0 << 2) | (0 << 1) | 1;
    // 0 = TOIE0
    TIMSK0 = 1;
}

void kbd_brightness_inc(void) {
    if (pwmval < 9) pwmval += 2;
    OCR0A = pwmval;
}

void kbd_brightness_dec(void) {
    if (pwmval > 1) pwmval -= 2;
    OCR0A = pwmval;
}

void kbd_brightness_set(uint8_t brite) {
    pwmval = brite;
    if (pwmval >= 10) pwmval = 10;
    OCR0A = pwmval;
}

static void local_test(void) {
    low_battery = !low_battery;
}

#ifdef KBD_VARIANT_STANDALONE
const menu_item_t circle_menu[] = {
    { "Exit Menu",        "ESC",  KEY_ESCAPE, true,   NULL },
    { "Key Light -",      "F1",   KEY_F1,     false,  ux_decrease_oled_brightness },
    { "Key Light +",      "F2",   KEY_F2,     false,  ux_increase_oled_brightness },
    { "System Status",    "S",    KEY_S,      true,   ux_show_status },
    { "",                 "",     KEY_Z,      true,   local_test },
    MENU_END
};
#else
const menu_item_t circle_menu[] = {
    { "Exit Menu",        "ESC",  KEY_ESCAPE, true,   NULL },
    { "Power On",         "1",    KEY_1,      true,   controller_turn_on_som },
    { "Power Off",        "0",    KEY_0,      true,   controller_turn_off_som },
    { "Reset",            "R",    KEY_R,      true,   controller_reset_som },
    { "Battery Status",   "B",    KEY_B,      true,   ux_show_battery },
    { "Menu Bright \x1f", "F1",   KEY_F1,     false,  ux_decrease_oled_brightness },
    { "Menu Bright \x1e", "F2",   KEY_F2,     false,  ux_increase_oled_brightness },
    { "Key Lights \x1f",  "F3",   KEY_F3,     false,  kbd_brightness_dec },
    { "Key Lights \x1e",  "F4",   KEY_F4,     false,  kbd_brightness_inc },
    { "Wake",             "SPC",  KEY_SPACE,  true,   controller_wake_som },
    { "System Status",    "S",    KEY_S,      true,   ux_show_status },
    { "Keyboard off",     "P",    KEY_P,      true,   controller_turn_off_keyboard },
    { "",                 "",     KEY_Z,      true,   local_test },
    MENU_END
};
#endif

// scan the keyboard matrix and record which keys are currently pressed,
// after debouncing. this takes over half a millisecond.
static void scan_keyboard_matrix(void) {
    static uint8_t debounce_history[MATRIX_COLUMNS * MATRIX_ROWS] = { 0, };

    // pull ROWs low one after the other
    uint8_t pressed_byte = 0, pressed_bit = 1, loc = 0;
    for (int y = 0; y < MATRIX_ROWS; y++) {
        switch (y) {
            case 0: output_low(PORTB, 6); break;
            case 1: output_low(PORTB, 5); break;
            case 2: output_low(PORTB, 4); break;
            case 3: output_low(PORTD, 7); break;
            case 4: output_low(PORTD, 6); break;
            case 5: output_low(PORTD, 4); break;
        }

        // check input COLs
        for (int x = 0; x < MATRIX_COLUMNS; x++) {
            uint8_t pressed = 0;

            // column pins are all over the place
            switch (x) {
                case 0:  pressed = !(PIND & (1 << 5)); break;
                case 1:  pressed = !(PINF & (1 << 7)); break;
                case 2:  pressed = !(PINE & (1 << 6)); break;
                case 3:  pressed = !(PINC & (1 << 7)); break;
                case 4:  pressed = !(PINB & (1 << 3)); break;
                case 5:  pressed = !(PINB & (1 << 2)); break;
                case 6:  pressed = !(PINB & (1 << 1)); break;
                case 7:  pressed = !(PINB & (1 << 0)); break;
                case 8:  pressed = !(PINF & (1 << 0)); break;
                case 9:  pressed = !(PINF & (1 << 1)); break;
                case 10: pressed = !(PINF & (1 << 4)); break;
                case 11: pressed = !(PINF & (1 << 5)); break;
                case 12: pressed = !(PINF & (1 << 6)); break;
                case 13: pressed = !(PINC & (1 << 6)); break;
            }

            // shift new state as bit into debounce "register"
            bool now_pressed = state[pressed_byte] & pressed_bit;
            debounce_history[loc] = (debounce_history[loc] << 1) | pressed;

            // transition to "pressed" only on the first down event after a
            // run (7) of unpressed scans. transition to "unpressed" only
            // after a run (8) of unpressed scans.
            if (debounce_history[loc] == 0x00 && now_pressed) {
                state[pressed_byte] &= ~pressed_bit;
            } else if (debounce_history[loc] == 0x01 && !now_pressed) {
                state[pressed_byte] |= pressed_bit;
            }

            loc++;
            pressed_bit <<= 1;
            if (!pressed_bit) {
                pressed_byte++;
                pressed_bit = 1;
            }
        }

        switch (y) {
            case 0: output_high(PORTB, 6); break;
            case 1: output_high(PORTB, 5); break;
            case 2: output_high(PORTB, 4); break;
            case 3: output_high(PORTD, 7); break;
            case 4: output_high(PORTD, 6); break;
            case 5: output_high(PORTD, 4); break;
        }
    }
}

// report the result of the latest scan_keyboard_matrix, and fill in a list
// of (up to MAX_CONCURRENT_KEYS) keys that are currently pressed. also
// handles the circle menu. returns the count of pressed keys.
static uint8_t scan_keyboard(uint8_t keys[]) {
    static bool in_circle_menu = false;
    static uint8_t previous_key = 0;
    static uint32_t last_key_ticks = 0;
    uint32_t now = timer_get_ticks();

    uint8_t count = 0;
    for (int i = 0; i < MATRIX_BYTES; i++) {
        if (!state[i]) continue;
        for (int bit = 0; bit < 8; bit++) {
            if ((state[i] & (1 << bit)) && count < MAX_CONCURRENT_KEYS) {
                keys[count++] = matrix[(i << 3) | bit];
            }
        }
    }

    if (count == 1 && keys[0] == KEY_CIRCLE && !in_circle_menu && !previous_key) {
        menu_start(circle_menu);
        in_circle_menu = true;
        previous_key = KEY_CIRCLE;
        last_key_ticks = now;
        return 0;
    }

    // once no more keys are pressed, clear the circle-menu state and be a
    // normal keyboard again.
    if (count == 0) previous_key = 0;

    if (in_circle_menu) {
        if (count == 1 && keys[0] != previous_key) {
            in_circle_menu = menu_key(keys[0]);
            previous_key = keys[0];
            last_key_ticks = now;
        } else if (count == 0 && timer_diff_msec(last_key_ticks, now) >= MENU_IDLE_MSEC) {
            in_circle_menu = false;
            menu_stop();
        }
        return 0;
    }

    // still holding down a key from when we were in circle menu mode? ignore.
    if (previous_key) return 0;

    return count;
}


int main(void) {
#ifdef KBD_VARIANT_QWERTY_US
    matrix[4 * MATRIX_COLUMNS + 1] = KEY_DELETE;
#endif
#ifdef KBD_VARIANT_NEO2
    matrix[3 * MATRIX_COLUMNS + 0] = HID_KEYBOARD_SC_CAPS_LOCK; // M3
    matrix[2 * MATRIX_COLUMNS + 13] = KEY_ENTER;
    matrix[3 * MATRIX_COLUMNS + 13] = KEY_BACKSLASH_AND_PIPE; // M3
#endif

    SetupHardware();

    // check for safe boot (holding down the circle key for 500msec)...
    // this is to cover a case where the firmware is messed up enough that
    // you can't even power up the laptop. hold down circle while the
    // keyboard is powering up, and it will at least boot the laptop and
    // allow you to reflash. :)
    uint8_t keys[MAX_CONCURRENT_KEYS];
    uint8_t safe_count = 0;
    scan_keyboard_matrix();
    while ((state[CIRCLE_BYTE] & CIRCLE_BIT) != 0 && safe_count < 50) {
        safe_count++;
        if (safe_count >= 50) {
            display_clear();
            display_write("safe boot");
            display_flush();
            Delay_MS(1000);
            controller_turn_on_som();
            break;
        }
        Delay_MS(10);
        scan_keyboard_matrix();
    }

#ifndef KBD_VARIANT_STANDALONE
    uint32_t last_battery_check = 0, last_low_battery_blink = 0;
    bool low_battery_alert_active = false;
#endif
    uint32_t ux_started = 0;

    while (true) {
        // do our own (non-usb) scan, too, so we can handle the circle menu
        // when the laptop is off :)
        scan_keyboard_matrix();
        scan_keyboard(keys);

        HID_Device_USBTask(&Keyboard_HID_Interface);
        USB_USBTask();

        // wake up every 1ms: we only spend 50% of the time sleeping, but
        // it's better than broccoli.
        sleep_mode();
        while ((timer_tick & 31) != 0) sleep_mode();

        uint32_t now = timer_get_ticks();

        // check if we should clear the display after a UX screen
        if (ux_is_on()) {
            if (ux_started == 0) {
                ux_started = now;
            } else if (timer_diff_msec(ux_started, now) >= UX_IDLE_MSEC) {
                ux_clear();
                ux_started = 0;
            }
        } else {
            ux_started = 0;
        }

#ifndef KBD_VARIANT_STANDALONE
        if (timer_diff_msec(last_battery_check, now) >= BATTERY_CHECK_MSEC) {
            last_battery_check = now;
            int was_low = low_battery;
            low_battery = controller_battery_is_low();

            if (was_low && !low_battery) {
                low_battery_alert_active = false;
                ux_blink_low_battery(false);
            }
        }

        if (low_battery) {
            if (low_battery_alert_active && timer_diff_msec(last_low_battery_blink, now) >= BATTERY_ALERT_ON_MSEC) {
                low_battery_alert_active = false;
                last_low_battery_blink = now;
                ux_blink_low_battery(false);
            }
            if (!low_battery_alert_active && timer_diff_msec(last_low_battery_blink, now) >= BATTERY_ALERT_OFF_MSEC) {
                low_battery_alert_active = true;
                last_low_battery_blink = now;
                ux_blink_low_battery(true);
            }
        }
#endif
    }
}

void SetupHardware(void) {
    // Disable watchdog if enabled by bootloader/fuses
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    // Disable clock division
    clock_prescale_set(clock_div_1);

    // declare port pins as inputs (0) and outputs (1)
    DDRB  = 0b11110000;
    DDRC  = 0b00000000;
    DDRD  = 0b11011001;
    DDRE  = 0b00000000;
    DDRF  = 0b00000000;

    // initial pin states
    PORTB = 0b10001111;
    PORTC = 0b11000000;
    PORTD = 0b00100000;
    PORTE = 0b01000000;
    PORTF = 0b11111111;

    // disable JTAG
    MCUCR |= (1 << JTD);
    MCUCR |= (1 << JTD);

    kbd_brightness_init();
    oled_init();
    display_init(&font_normal);
    hid_report_init();

    ux_splash();

    controller_init();
    USB_Init();

    GlobalInterruptEnable();
    set_sleep_mode(SLEEP_MODE_IDLE);
}

ISR(WDT_vect)
{
  // WDT interrupt enable and flag cleared on entry
  wdt_disable(); // Disable watchdog for now
}


/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool ConfigSuccess = true;

  ConfigSuccess &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);

  USB_Device_EnableSOFEvents();
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
  HID_Device_MillisecondElapsed(&Keyboard_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(
    USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
    uint8_t* const ReportID,
    const uint8_t ReportType,
    void* ReportData,
    uint16_t* const ReportSize
) {
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;

    uint8_t count = scan_keyboard(KeyboardReport->KeyCode);
    if (count == 0) memset(KeyboardReport->KeyCode, 0, MAX_CONCURRENT_KEYS);

    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;
}
