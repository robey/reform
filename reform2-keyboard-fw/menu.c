/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#include <stdlib.h>
#include "display.h"
#include "keyboard.h"
#include "scancodes.h"
#include "menu.h"

// menu system
static const menu_item_t *items = NULL;
static uint8_t current = 0;
static uint8_t scroll_top = 0;

static void render(void) {
    display_clear();
    uint8_t row = scroll_top;
    for (int y = 0; y < display_text_height(); y++) {
        // a blank description is a "hidden" menu item, accessible only via its key
        while (items[row].label != NULL && items[row].label[0] == 0) row++;
        if (items[row].label == NULL) break;  // end of menu

        display_move(0, y);
        display_write(items[row].label);
        if (items[row].key_label != NULL) {
            display_move(display_text_width() - strlen(items[row].key_label), y);
            display_write(items[row].key_label);
        }
        if (row == current) display_invert_line(y);
        row++;
    }
    display_flush();
}

void menu_start(const menu_item_t *menu) {
    items = menu;
    current = 0;
    scroll_top = 0;
    render();
}

void menu_stop(void) {
    items = NULL;
    display_clear();
    display_flush();
}

bool menu_is_active(void) {
    return items != NULL;
}

static bool menu_action(uint8_t index) {
    if (!items[index].action) {
        // no action = exit menu
        menu_stop();
        return false;
    }

    items[index].action();
    if (items[index].terminate_menu) {
        // don't clear the screen: might have been a display request
        items = NULL;
        return false;
    }
    return true;
}

// returns true to stay in menu mode
bool menu_key(uint8_t key) {
    uint8_t prev_row = current;

    // navigation?
    switch (key) {
        case KEY_UP_ARROW:
            if (current > 0) current--;
            while (current > 0 && items[current].label[0] == 0) current--;
            if (current == 0 && items[current].label[0] == 0) current = prev_row;
            while (current < scroll_top) scroll_top--;
            render();
            return true;
        case KEY_DOWN_ARROW:
            current++;
            while (items[current].label != NULL && items[current].label[0] == 0) current++;
            if (items[current].label == NULL) current = prev_row;
            while (current >= scroll_top + display_text_height()) scroll_top++;
            render();
            return true;
        case KEY_ENTER:
            return menu_action(current);
        case KEY_ESCAPE:
        case KEY_CIRCLE:
            menu_stop();
            return false;
        default:
            for (int i = 0; items[i].label != NULL; i++) {
                if (items[i].keycode == key) return menu_action(i);
            }
            // exit quietly
            menu_stop();
            return false;
    }
}
