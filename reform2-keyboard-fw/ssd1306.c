#include <string.h>
#include <avr/pgmspace.h>
#include "i2c.h"
#include "ssd1306.h"

// in case you want to rotate the screen 180 degrees (half-tau)
#define ROTATE 0

// Write command sequence.
static inline void send_cmd1(uint8_t cmd) {
    i2c_start_write(SSD1306_ADDRESS);
    i2c_master_write(0x0);  // Command byte
    i2c_master_write(cmd);
    i2c_master_stop();
}

// Write 2-byte command sequence.
static inline void send_cmd2(uint8_t cmd, uint8_t opr) {
    send_cmd1(cmd);
    send_cmd1(opr);
}

// Write 3-byte command sequence.
static inline void send_cmd3(uint8_t cmd, uint8_t opr1, uint8_t opr2) {
    send_cmd1(cmd);
    send_cmd1(opr1);
    send_cmd1(opr2);
}

void oled_init(void) {
    i2c_master_init();
    send_cmd1(DisplayOff);
    send_cmd2(SetDisplayClockDiv, 0x80);
    send_cmd2(SetMultiPlex, DisplayHeight - 1);

    send_cmd2(SetDisplayOffset, 0);

    send_cmd1(SetStartLine | 0x0);
    send_cmd2(SetChargePump, 0x14 /* Enable */);
    send_cmd2(SetMemoryMode, 0 /* horizontal addressing */);

#if ROTATE
    // the following Flip the display orientation 180 degrees
    send_cmd1(SegRemap);
    send_cmd1(ComScanInc);
#else
    // Flips the display orientation 0 degrees
    send_cmd1(SegRemap | 0x1);
    send_cmd1(ComScanDec);
#endif

    send_cmd2(SetComPins, 0x2);
    send_cmd2(SetContrast, 0x8f);
    send_cmd2(SetPreCharge, 0xf1);
    send_cmd2(SetVComDetect, 0x40);
    send_cmd1(DisplayAllOnResume);
    send_cmd1(NormalDisplay);
    send_cmd1(DeActivateScroll);
    send_cmd1(DisplayOn);

    send_cmd2(SetContrast, 0); // Dim

    // Clear all of the display bits (there can be random noise
    // in the RAM on startup)
    oled_clear();
}

void oled_clear(void) {
    send_cmd3(PageAddr, 0, (DisplayHeight / 8) - 1);
    send_cmd3(ColumnAddr, 0, DisplayWidth - 1);

    i2c_start_write(SSD1306_ADDRESS);
    i2c_master_write(0x40);
    for (int i = 0; i < DisplayHeight * DisplayWidth / 8; i++) {
        i2c_master_write(0);
    }
    i2c_master_stop();
}

void oled_off(void) {
    send_cmd1(DisplayOff);
}

void oled_on(void) {
    send_cmd1(NormalDisplay);
    send_cmd1(DisplayOn);
}

void oled_contrast(int c) {
    send_cmd2(SetContrast, c);
}

void oled_home(void) {
    // Move to the home position
    send_cmd3(PageAddr, 0, (DisplayHeight / 8) - 1);
    send_cmd3(ColumnAddr, 0, DisplayWidth - 1);
}

void oled_start_render(void) {
    i2c_start_write(SSD1306_ADDRESS);
    i2c_master_write(0x40);
}

void oled_finish_render(void) {
    i2c_master_stop();
}
