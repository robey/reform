/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#include <string.h>
#include <avr/pgmspace.h>
#include "controller.h"
#include "display.h"
#include "ssd1306.h"
#include "ux.h"

// change this to 0 to use the original MNT font
#define USE_ORANJ_FONT 1

#if USE_ORANJ_FONT
#include "assets/oranj_reform.h"
#else
#include "assets/font.h"
#endif
#include "assets/bizcat_reform.h"

#if USE_ORANJ_FONT
const monospace_font_t font_normal = { oranj_reform_font_data, 8, 6 };
#else
const monospace_font_t font_normal = { orig_font, 8, 6 };
#endif
const monospace_font_t font_bizcat = { bizcat_reform_font_data, 16, 8 };

static uint8_t framebuffer[DisplayHeight * DisplayWidth / 8] = { 0, };

// text grid
static uint8_t text_cells[MAX_HEIGHT * MAX_WIDTH];
static uint8_t text_invert[(MAX_HEIGHT * MAX_WIDTH + 7) / 8];
static const monospace_font_t *text_font = &font_normal;
static uint8_t text_height = 0;
static uint8_t text_width = 0;
static uint8_t text_cursor = 0;
static bool text_dirty = false;
static bool text_inverting = false;

void display_init(const monospace_font_t *font) {
    text_font = font;
    text_height = DisplayHeight / text_font->height;
    text_width = DisplayWidth / text_font->width;
    display_clear();
}

void display_clear(void) {
    memset(framebuffer, 0, sizeof(framebuffer));
    memset(text_cells, ' ', sizeof(text_cells));
    memset(text_invert, 0, sizeof(text_invert));
    text_cursor = 0;
    text_dirty = true;
    text_inverting = false;
}

void display_clear_line(uint8_t line) {
    memset(text_cells + line * text_width, ' ', text_width);
    for (int i = line * text_width; i < (line + 1) * text_width; i++) {
        text_invert[i >> 3] &= ~(1 << (i & 7));
    }
}

void display_invert_line(uint8_t line) {
    for (int i = line * text_width; i < (line + 1) * text_width; i++) {
        text_invert[i >> 3] |= (1 << (i & 7));
    }
}

uint8_t display_text_width(void) {
    return text_width;
}

uint8_t display_text_height(void) {
    return text_height;
}

// scroll up one line.
void display_scroll(void) {
    memmove(text_cells, text_cells + text_width, (text_height - 1) * text_width);

    // this is a pain: scroll the invert bits too.
    uint8_t bytes = text_width >> 3;
    uint8_t bits = text_width & 7;
    for (int i = 0; i < (text_width * (text_height - 1) + 7) >> 3; i++) {
        text_invert[i] = (text_invert[i + bytes] >> bits) + (text_invert[i + bytes + 1] << (8 - bits));
    }

    display_clear_line(text_height - 1);
    text_dirty = true;
}

// escape commands to display certain glyphs or change state
#define ESC_CLEAR       'C'
#define ESC_LF          'n'
#define ESC_INVERT      'I'
#define ESC_NORMAL      'N'
#define ESC_BATTERY     'B'
#define ESC_CHARGING    'A'
#define ESC_PERCENT     'P'
#define ESC_FONT_BIG    '+'
#define ESC_FONT_NORMAL '-'

#define CHAR_LIGHTNING  0x0c

void display_putc(uint8_t c) {
    while (text_cursor >= text_width * text_height) {
        text_cursor -= text_width;
        display_scroll();
    }
    text_cells[text_cursor] = c;
    uint8_t invert_bit = 1 << (text_cursor & 7);
    text_invert[text_cursor >> 3] &= ~invert_bit;
    text_invert[text_cursor >> 3] |= text_inverting ? invert_bit : 0;
    text_cursor++;
    text_dirty = true;
}

void display_write_len(const char *text, int len) {
    bool escaped = false;
    char buffer[4];
    battery_info_t info;
    info.total_percent = 255;

    for (int i = 0; i < len; i++) {
        if (escaped) {
            switch (text[i]) {
                case ESC_CLEAR:
                    display_clear();
                    break;
                case ESC_LF:
                    text_cursor = ((text_cursor / text_width) + 1) * text_width;
                    break;
                case ESC_INVERT:
                    text_inverting = true;
                    break;
                case ESC_NORMAL:
                    text_inverting = false;
                    break;
                case ESC_FONT_BIG:
                    display_init(&font_bizcat);
                    break;
                case ESC_FONT_NORMAL:
                    display_init(&font_normal);
                    break;
                case ESC_BATTERY:
                    if (info.total_percent == 255) controller_probe_batteries(&info);
                    ux_get_battery_icon(buffer, info.total_percent);
                    display_putc(buffer[0]);
                    display_putc(buffer[1]);
                    break;
                case ESC_CHARGING:
                    if (info.total_percent == 255) controller_probe_batteries(&info);
                    display_putc(info.total_amps < 0 ? CHAR_LIGHTNING : ' ');
                    break;
                case ESC_PERCENT:
                    if (info.total_percent == 255) controller_probe_batteries(&info);
                    sprintf(buffer, "%3d", info.total_percent);
                    display_putc(buffer[0]);
                    display_putc(buffer[1]);
                    display_putc(buffer[2]);
                    break;
                case 0xff:
                    display_putc(0xff);
                    break;
            }

            escaped = false;
            continue;
        }

        switch (text[i]) {
            case '\n':
                text_cursor = ((text_cursor / text_width) + 1) * text_width;
                break;
            case 0xff:
                escaped = true;
                break;
            default:
                display_putc(text[i]);
        }
    }
}

void display_write(const char *text) {
    display_write_len(text, strlen(text));
}

void display_move(uint8_t x, uint8_t y) {
    text_cursor = text_width * y + x;
    if (text_cursor >= text_width * text_height) text_cursor = 0;
}

void display_invert(bool invert) {
    text_inverting = invert;
}

static void render(void) {
    oled_on();
    oled_home();
    oled_start_render();
    for (int i = 0; i < DisplayWidth * DisplayHeight / 8; i++) oled_render(framebuffer[i]);
    oled_finish_render();
}

void display_refresh(void) {
    render();
}

void display_flush(void) {
    if (!text_dirty) return;
    uint8_t x_offset = (DisplayWidth - text_width * text_font->width) / 2;
    int stride = (text_font->height + 7) / 8;

    int i = 0;
    uint8_t cursor = 0;
    for (uint8_t y = 0; y < text_height; y++) {
        for (uint8_t row = 0; row < stride; row++) {
            // back up if we're drawing the later row of a tall font
            if (row > 0) cursor -= text_width;

            for (uint8_t pad = 0; pad < x_offset; pad++) framebuffer[i++] = 0;
            for (uint8_t x = 0; x < text_width; x++) {
                int offset = (text_cells[cursor] & 0x7f) * text_font->width * stride;
                for (uint8_t column = 0; column < text_font->width; column++) {
                    uint8_t pixels = pgm_read_byte((uint8_t *)text_font->glyphs + offset + column * stride + row);
                    if (text_invert[cursor >> 3] & (1 << (cursor & 7))) pixels = ~pixels;
                    framebuffer[i++] = pixels;
                }
                cursor++;
            }
        }
        for (uint8_t pad = 0; pad < x_offset; pad++) framebuffer[i++] = 0;
    }

    render();
    text_dirty = false;
}

void display_bitmap(uint16_t offset, const uint8_t *data, uint16_t len) {
    uint16_t max_len = sizeof(framebuffer) - offset;
    if (len > max_len) len = max_len;
    memmove(framebuffer + offset, data, len);
    render();
}

static void check_effect(int i) {
    if ((i & 7) != 0 || i >= sizeof(framebuffer)) return;
    // draw a cursor, then render
    memset(framebuffer + i, 255, 8);
    render();
}

static uint8_t data_read_byte(const uint8_t *p) {
    return *p;
}

static uint8_t code_read_byte(const uint8_t *p) {
    return pgm_read_byte(p);
}

// a very simple RLE encoding scheme that works well with simple images:
// each frame begins with one byte `tnnnnnnn`:
//   - t=0 : n is the number of uncompressed bytes that follow
//   - t=1 : n is the number of times to repeat the next byte
// the "packbmp.py" script will generate an RLE encoding for a display-sized
// image, if you want to replace one of the splash screens.
static void _display_packed_bitmap(
    uint8_t (*reader)(const uint8_t *p),
    uint16_t offset,
    const uint8_t *data,
    uint16_t len,
    bool cursor_effect
) {
    int i = offset;
    const uint8_t *end = data + len;
    while (i < sizeof(framebuffer) && data < end) {
        uint8_t header = reader(data++);
        if (header == 0 || data >= end) break;
        uint8_t n = header & 0x7f;
        if (header & 0x80) {
            uint8_t repeat = reader(data++);
            for (int j = 0; j < n; j++) {
                if (i >= sizeof(framebuffer)) break;
                framebuffer[i++] = repeat;
                if (cursor_effect) check_effect(i);
            }
        } else {
            for (int j = 0; j < n; j++) {
                if (i >= sizeof(framebuffer) || data >= end) break;
                framebuffer[i++] = reader(data++);
                if (cursor_effect) check_effect(i);
            }
        }
    }
    render();
}

void display_packed_bitmap(uint16_t offset, const uint8_t *data, uint16_t len) {
    _display_packed_bitmap(data_read_byte, offset, data, len, false);
}

void display_packed_bitmap_pgm(uint16_t offset, const uint8_t *data, uint16_t len, bool cursor_effect) {
    _display_packed_bitmap(code_read_byte, offset, data, len, cursor_effect);
}

// low-level overwrite the framebuffer for an alert like "low battery"
void display_blit_packed_bitmap_pgm(const uint8_t *data, uint16_t len) {
    oled_on();
    oled_home();
    oled_start_render();

    int i = 0;
    const uint8_t *end = data + len;
    while (i < DisplayWidth * DisplayHeight && data < end) {
        uint8_t header = pgm_read_byte(data++);
        if (header == 0 || data >= end) break;
        uint8_t n = header & 0x7f;
        if (header & 0x80) {
            uint8_t repeat = pgm_read_byte(data++);
            for (int j = 0; j < n; j++) {
                if (i >= sizeof(framebuffer)) break;
                oled_render(repeat);
            }
        } else {
            for (int j = 0; j < n; j++) {
                if (i >= sizeof(framebuffer) || data >= end) break;
                oled_render(pgm_read_byte(data++));
            }
        }
    }
    oled_finish_render();
}
