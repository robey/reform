#!/bin/sh
PYTHON=python
if ! $PYTHON --version >/dev/null 2>&1; then
    PYTHON=python3
fi

$PYTHON $(dirname $0)/packbmp.py "$@"
