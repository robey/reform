/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

// This file contains all the code for communicating with the system
// controller, which has things like battery status.

#include <stdlib.h>
#include <avr/sleep.h>
#include "LUFA/Drivers/Peripheral/Serial.h"
#include "controller.h"
#include "display.h"
#include "keyboard.h"
#include "ssd1306.h"
#include "ux.h"

// how long do we wait (busy loops) before giving up on a response
#define TIMEOUT (500000)

// anything received from the last command
static uint8_t recv_buffer[64];

void controller_init(void) {
    Serial_Init(57600, false);
    recv_buffer[0] = 0;
}

// wait for the serial port to be quiet.
static void flush(void) {
    for (int i = 0; i < 100 && Serial_ReceiveByte() >= 0; i++);
}

// returns true on success, false on timeout.
static bool receive_response(void) {
    int n = 0;
    while (true) {
        int16_t c = -1;
        for (int i = 0; i < TIMEOUT && c <= 0; i++) c = Serial_ReceiveByte();
        if (c <= 0) {
            flush();
            return false;
        }

        if (c == '\r') {
            recv_buffer[n] = 0;
            flush();
            return true;
        }
        recv_buffer[n++] = c;
        if (n >= 64) n = 0;
    }
}

// don't wait for a response
static void controller_send(const char *command) {
    flush();
    while (*command) Serial_SendByte(*command++);
    Serial_SendByte('\r');
    Delay_MS(1);
}

const char *controller_request(const char *command) {
    controller_send(command);
    if (receive_response()) {
        return (const char *)recv_buffer;
    } else {
        return NULL;
    }
}

bool controller_probe_batteries(battery_info_t *info) {
    memset(info->decivolts, 0, 8);
    info->total_amps = info->total_volts = 0;
    info->total_percent = 0;

    const char *response = controller_request("c");
    if (response == NULL) return false;

    // lpc format: 32 32 32 32 32 32 32 32 mA 0256mV26143 ???%
    //             |  |  |  |  |  |  |  |  | |      |     |
    //             0  3  6  9  12 15 18 21 24|      |     |
    //                                       26     33    39
    //                                       |
    //                                       `- can be a minus
#define amps_offset (3 * 8 + 2)
#define volts_offset (amps_offset + 5 + 2)
#define battery_offset (volts_offset + 6)

    for (int i = 0; i < 8; i++) {
        info->decivolts[i] = (response[i * 3] - '0') * 10 + response[i * 3 + 1] - '0';
        if (info->decivolts[i] > 99) info->decivolts[i] = 0;  // garbage data.
    }

    const char *bat_gauge = &response[battery_offset];
    while (*bat_gauge == ' ') bat_gauge++;
    info->total_amps = ((float)atoi(&response[amps_offset])) / 1000.0;
    info->total_volts = ((float)atoi(&response[volts_offset])) / 1000.0;
    info->total_percent = atoi(bat_gauge);
    return true;
}

// is the battery "alarmingly low"?
bool controller_battery_is_low(void) {
    battery_info_t info;
    if (!controller_probe_batteries(&info)) return false;

    for (int i = 0; i < 8; i++) {
        if (info.decivolts[i] < 30) return true;
    }

    if (info.total_percent > 0 && info.total_percent < 10) return true;
    return false;
}

void controller_turn_on_som(void) {
    display_clear();
    display_flush();
    controller_send("1p");
    flush();
    ux_splash();
    kbd_brightness_init();
}

void controller_turn_off_som(void) {
    ux_unsplash();
    controller_send("0p");
    flush();
    controller_turn_off_keyboard();
}

void controller_reset_som(void) {
    display_clear();
    display_flush();
    controller_send("2p");
    flush();
}

void controller_wake_som(void) {
    controller_send("1w");
    flush();
    controller_send("0w");
    flush();
}

/*
 * Setup the AVR to enter the Power-Down state to greatly save power.
 * Configures all outputs to be in the low state if possible, and disables
 * services like USB and Serial.
 *
 * Will leave the ports setup so that the Circle key row is being scanned
 * so when the watchdog wakes up it can quickly check and go back to sleep if not
 * Added by Chartreuse - 2021/08/14
 */
void controller_turn_off_keyboard(void) {
    USB_Disable(); // Stop USB stack so it doesn't wake us up

    kbd_brightness_set(0);
    // Turn off OLED to save power
    display_clear();
    display_flush();
    oled_off();
    // Disable ADC to save even more power
    ADCSRA = 0;

    cli();    // No interrupts

    // Set all ports not floating if possible, leaving pullups alone
    PORTB = 0x3F; // Leave pull-up on all the columns on PB0-3, drive rows 2-3 high, 1-low
    PORTC = 0xC0;
    PORTD = 0xF0; // Keep pullup on PD5 like setup did, drive rows 4,5,6 high
    PORTE = 0x40; // Pullup on PE6
    PORTF = 0xFF; // Pullups on PF (columns)
    // ROW1 is the only row driven low and left low, thus is always ready to be read out
    // We just need to check COL14 (PC6) if it is low (pressed) or high

    // Unfortunately the circle key is on COL14(PC6) which doesn't have pin change interrupt
    // capabilities, so we need to wake up every so often to check if it is pressed, and
    // if so bring us out of power-off.
    // We can use the Watchdog timer to do this.

    do {
        wdt_reset();
        WDTCSR = (1<<WDCE) | (1<<WDE); // Enable writes to watchdog
        WDTCSR = (1<<WDIE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1); // Interrupt mode, 1s timeout

        // Enter Power-save mode
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
        sei();              // Enable interrupts so we can actually wake
        sleep_cpu();        // Actually go to sleep
        // Zzzzzz
        sleep_disable();    // We've woken up
        sei();
        // Check if circle key has been pressed (active-low)
        // If not reset the watchdog and try again
    } while (PINC & (1 << 6));

    // Resume and reinitialize hardware
    SetupHardware();
}

void controller_turn_off_aux(void) {
    controller_send("3p");
    flush();
}

void controller_turn_on_aux(void) {
    controller_send("4p");
    flush();
}

void controller_enable_som_uart(void) {
    controller_send("1u");
    flush();
}

void controller_disable_som_uart(void) {
    controller_send("0u");
    flush();
}
