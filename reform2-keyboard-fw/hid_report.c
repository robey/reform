/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#include "Config/LUFAConfig.h"
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>
#include "controller.h"
#include "display.h"
#include "keyboard.h"
#include "menu.h"
#include "ux.h"

// hid commands are all 4-letter
#define cmd(_s) (*(uint32_t *)(_s))
#define CMD_TEXT_FRAME      cmd("OLED")     // fill the screen with a single wall of text
#define CMD_TEXT_WRITE      cmd("WRIT")     // write to the oled as if it's a console
#define CMD_TEXT_FONT       cmd("FONT")     // ("0" or "1") switch between text fonts
#define CMD_REPORT_POWER    cmd("RPRT")     // ask for power stats
#define CMD_UX_POWER        cmd("UXPW")     // display detailed power screen as if user hit circle-B
#define CMD_UX_STATUS       cmd("UXST")     // display status screen as if user hit circle-S
#define CMD_OLED_CLEAR      cmd("WCLR")     // clear the oled display
#define CMD_OLED_BITMAP     cmd("WBIT")     // (u16 offset, u8 bytes...) write raw bytes into the oled framebuffer
#define CMD_OLED_RLE        cmd("WRLE")     // (u16 offset, u8 bytes...) write RLE-encoded bytes into the oled framebuffer
#define CMD_POWER_OFF       cmd("PWR0")     // turn off power rails
#define CMD_LIGHT           cmd("LITE")     // keyboard backlight
#define CMD_UART_ON         cmd("UAR1")     // uart reporting on
#define CMD_UART_OFF        cmd("UAR0")     // uart reporting off
#define CMD_AUX_ON          cmd("PWR4")     // turn on aux power
#define CMD_AUX_OFF         cmd("PWR3")     // turn off aux power

#define CMD_TEST cmd("test")

void hid_report_init(void) {
    // pass
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(
    USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
    const uint8_t ReportID,
    const uint8_t ReportType,
    const void* report_data,
    const uint16_t report_size
) {
    const uint8_t *data = (uint8_t *)report_data;

    if (report_size < 4) return;
    const uint32_t command = *(uint32_t *)data;

    if (command == CMD_TEXT_FRAME) {
        if (!menu_is_active()) {
            if (ux_is_on()) ux_clear();
            display_clear();
            for (int i = 4; i < report_size; i++) display_putc(data[i]);
            display_flush();
        }
    } else if (command == CMD_TEXT_WRITE) {
        if (!menu_is_active()) {
            if (ux_is_on()) ux_clear();
            display_write_len((const char *)&data[4], report_size - 4);
            display_flush();
        }
    } else if (command == CMD_TEXT_FONT) {
        if (!menu_is_active()) {
            if (ux_is_on()) ux_clear();
            display_init(report_size > 4 && data[4] == '1' ? &font_bizcat : &font_normal);
            display_flush();
        }
    } else if (command == CMD_REPORT_POWER) {
        if (!menu_is_active()) {
            if (ux_is_on()) ux_clear();
            const char *report = controller_request("0c");
            display_clear();
            if (report) display_write(report);
            display_flush();
        }
    } else if (command == CMD_UX_POWER) {
        ux_show_battery();
    } else if (command == CMD_UX_STATUS) {
        ux_show_status();
    } else if (command == CMD_OLED_CLEAR) {
        if (!menu_is_active()) {
            if (ux_is_on()) ux_clear();
            display_clear();
            display_flush();
        }
    } else if (command == CMD_OLED_BITMAP) {
        if (!menu_is_active() && report_size > 6) {
            if (ux_is_on()) ux_clear();
            uint16_t offset = (uint16_t)data[4] + (((uint16_t)data[5]) << 8);
            display_bitmap(offset, data + 6, report_size - 6);
        }
    } else if (command == CMD_OLED_RLE) {
        if (!menu_is_active() && report_size > 6) {
            if (ux_is_on()) ux_clear();
            uint16_t offset = (uint16_t)data[4] + (((uint16_t)data[5]) << 8);
            display_packed_bitmap(offset, data + 6, report_size - 6);
        }
    } else if (command == CMD_POWER_OFF) {
        controller_turn_off_som();
    } else if (command == CMD_LIGHT) {
        kbd_brightness_set(report_size > 4 ? data[4] - '0' + 1 : 0);
    } else if (command == CMD_UART_ON) {
        controller_enable_som_uart();
    } else if (command == CMD_UART_OFF) {
        controller_disable_som_uart();
    } else if (command == CMD_AUX_ON) {
        controller_turn_on_aux();
    } else if (command == CMD_AUX_OFF) {
        controller_turn_off_aux();
    }
}
