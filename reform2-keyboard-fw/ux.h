/*
  MNT Reform 2.0 Keyboard Firmware
  Copyright 2019-2021  Lukas F. Hartmann / MNT Research GmbH, Berlin
  lukas@mntre.com
  Additional work: Robey Pointer robey@lag.net
*/

#pragma once

void ux_increase_oled_brightness(void);
void ux_decrease_oled_brightness(void);
void ux_get_battery_icon(char *buffer, uint8_t percent);
void ux_show_battery(void);
void ux_show_status(void);
void ux_blink_low_battery(bool blink);
void ux_splash(void);
void ux_unsplash(void);
bool ux_is_on(void);
void ux_clear(void);
